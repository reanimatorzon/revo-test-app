package com.revolut.java.test.repo;

import static com.revolut.java.test.repo.Repository.SQL_DECREMENT_BALANCE_BY_ID_AND_SUITABLE_BALANCE;
import static com.revolut.java.test.repo.Repository.SQL_GET_ACCOUNT_BY_ID;
import static com.revolut.java.test.repo.Repository.SQL_INCREMENT_BALANCE_BY_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.revolut.java.test.dto.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RepositoryTest {

    @Mock
    private Connection connection;

    @Mock
    private PreparedStatement getAccountByIdStatement;

    @Mock
    private PreparedStatement decrementBalanceByIdAndSuitableBalance;

    @Mock
    private PreparedStatement incrementBalanceById;

    private Repository repository;

    @BeforeEach
    void setUp() throws SQLException {

        doReturn(getAccountByIdStatement).when(connection).prepareStatement(SQL_GET_ACCOUNT_BY_ID);
        doReturn(decrementBalanceByIdAndSuitableBalance).when(connection).prepareStatement(SQL_DECREMENT_BALANCE_BY_ID_AND_SUITABLE_BALANCE);
        doReturn(incrementBalanceById).when(connection).prepareStatement(SQL_INCREMENT_BALANCE_BY_ID);

        Repository.initialize(connection);

        repository = Repository.getInstance();
        repository.twoSeconds = 0;

    }

    @Test
    void testGetAccount() throws Repository.RepositoryException, SQLException {

        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getInt("id")).thenReturn(7);
        when(resultSet.getString("name")).thenReturn("Name");
        when(resultSet.getLong("balance")).thenReturn(777L);

        when(getAccountByIdStatement.executeQuery()).thenReturn(resultSet);

        Account account = repository.getAccount(1);

        assertEquals(Integer.valueOf(7), account.getId());
        assertEquals("Name", account.getName());
        assertEquals(Long.valueOf(777), account.getBalance());
    }

    @Test
    void testTransferLackOfMoney() throws SQLException {

        when(decrementBalanceByIdAndSuitableBalance.executeUpdate()).thenReturn(0);
        assertEquals(
            "No account to withdraw money or not enough balance",
            repository.transfer(1, 2, 1000));

        verify(decrementBalanceByIdAndSuitableBalance).executeUpdate();
        verify(connection).rollback();

        verify(incrementBalanceById, never()).executeUpdate();
        verify(connection, never()).commit();

    }

    @Test
    void testTransferNoDestinationAccount() throws SQLException {

        when(decrementBalanceByIdAndSuitableBalance.executeUpdate()).thenReturn(1);
        when(incrementBalanceById.executeUpdate()).thenReturn(0);
        assertEquals(
            "No destination account found",
            repository.transfer(1, 2, 1000));

        verify(decrementBalanceByIdAndSuitableBalance).executeUpdate();
        verify(incrementBalanceById).executeUpdate();
        verify(connection).rollback();

        verify(connection, never()).commit();
    }

    @Test
    void testTransferOk() throws SQLException {

        when(decrementBalanceByIdAndSuitableBalance.executeUpdate()).thenReturn(1);
        when(incrementBalanceById.executeUpdate()).thenReturn(1);
        assertNull(repository.transfer(1, 2, 1000));

        verify(decrementBalanceByIdAndSuitableBalance).executeUpdate();
        verify(incrementBalanceById).executeUpdate();
        verify(connection).commit();

        verify(connection, never()).rollback();

    }

}