package com.revolut.java.test;

import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.port;

import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

import com.revolut.java.test.api.AccountApi;
import com.revolut.java.test.api.TransferApi;
import com.revolut.java.test.repo.Repository;
import org.h2.tools.RunScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.utils.StringUtils;

public class App {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class.getName());

    private static Connection connection;

    public static void main(String[] args) {
        port(args.length > 0 ? Integer.valueOf(args[0]) : 4567);
        connection = setupDatabase();
        initRepository(connection);
        configureRoutes();
    }

    private static void configureRoutes() {
        path("/v1", () -> {
            before("/*", (req, res) -> res.type("application/json"));
            get("/account/:id/balance", AccountApi::getAccountBalanceById);
            get("/transfer/:from/:to/:amount", TransferApi::transfer); // GET for ease of playing with
        });
    }

    private static Connection setupDatabase() {

        try {
            Class.forName("org.h2.Driver"); // load driver
        } catch (ClassNotFoundException e) {
            halt("No H2 driver found!", e);
        }

        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:h2:./revolut;AUTO_SERVER=TRUE;LOCK_TIMEOUT=10000");
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            halt("There is an error during database configuration!", e);
        }

        boolean schemaExists = checkSchemaExists(Objects.requireNonNull(connection));

        if (!schemaExists) {
            try {
                RunScript.execute(connection,
                    new InputStreamReader(App.class.getClassLoader().getResourceAsStream("init.sql")));
                connection.commit();
            } catch (SQLException e) {
                halt("Initial database script failed!", e);
            }
        }

        return connection;

    }

    private static void initRepository(Connection connection) {
        Repository repository = Repository.initialize(connection);
        if (repository == null) {
            // already logged on the previous level
            System.exit(1);
        }
    }

    /**
     * Returns schema exists.
     * (!) I believe, there is "beautiful" way to do that, but I decided to not waste time finding it.
     */
    private static boolean checkSchemaExists(Connection connection) {
        try {
            return connection.getMetaData().getTables(connection.getCatalog(), null, "ACCOUNT", null).next();
        } catch (SQLException e) {
            LOGGER.error("There is an error during database configuration!", e);
            System.exit(1);
        }
        return false;
    }

    /**
     * Closes the database connection and stops the application.
     */
    private static void halt(String message, Throwable throwable) {
        if (StringUtils.isNotBlank(message)) {
            LOGGER.error(message, throwable);
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error("Closing connection fails during System.exit()");
            }
        }
        System.exit(1);
    }

}
