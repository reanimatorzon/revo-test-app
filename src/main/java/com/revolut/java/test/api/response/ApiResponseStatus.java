package com.revolut.java.test.api.response;

enum ApiResponseStatus {

    OK,
    FAIL;

}
