package com.revolut.java.test.api;

import com.google.common.collect.ImmutableMap;
import com.revolut.java.test.api.response.ApiResponse;
import com.revolut.java.test.dto.Account;
import com.revolut.java.test.repo.Repository;
import spark.Request;
import spark.Response;

public class AccountApi {

    private static Repository repository = Repository.getInstance();

    private AccountApi() {
    }

    public static ApiResponse getAccountBalanceById(Request req, Response res) {

        Integer accountId = Integer.valueOf(req.params("id"));

        Account account;
        try {
            account = repository.getAccount(accountId);
        } catch (Repository.RepositoryException e) {
            return ApiResponse.fail(e.getMessage());
        }

        if (account == null) {
            return ApiResponse.fail(String.format("No account with id = %s found!", accountId));
        }

        return ApiResponse.ok(ImmutableMap.of(
            "name", account.getName(),
            "balance", account.getBalance()
        ));

    }

}
