package com.revolut.java.test.api;

import com.revolut.java.test.api.response.ApiResponse;
import com.revolut.java.test.dto.Account;
import com.revolut.java.test.repo.Repository;
import spark.Request;
import spark.Response;

import java.util.function.Predicate;

public class TransferApi {

    private static Repository repository = Repository.getInstance();

    private static final int MAX_RETRY_COUNT = 3;

    private TransferApi() {
    }

    public static ApiResponse transfer(Request req, Response res) {

        Integer fromAccountId;
        Integer toAccountId;
        Integer amount;
        try {
            fromAccountId = Integer.valueOf(req.params("from"));
            toAccountId = Integer.valueOf(req.params("to"));
            amount = Integer.valueOf(req.params("amount"));
        } catch (NumberFormatException e) {
            return ApiResponse.fail("Non-integer api parameters");
        }

        // ordinary retry implementation for concurrent update
        int retry = MAX_RETRY_COUNT;

        while (retry > 0) {

            try {

                Account fromAccount = repository.getAccount(fromAccountId);
                if (fromAccount == null) {
                    return ApiResponse.fail(String.format("No sender account with id = %d", fromAccountId));
                }
                if (fromAccount.getBalance() <= amount) {
                    return ApiResponse.fail("Not enough balance on sender account");
                }

                String errorMessage = repository.transfer(fromAccountId, toAccountId, amount);
                if (errorMessage == null) {
                    return ApiResponse.ok("Successfully transferred");
                } else if (errorMessage.equals(Repository.TRANSFER_CONCURRENT_UPDATE_RESPONSE)) {
                    --retry;
                } else {
                    return ApiResponse.fail(String.format("Money transaction failed cause '%s'", errorMessage));
                }

            } catch (Repository.RepositoryException e) {
                return ApiResponse.fail("Non-integer api parameters");
            }

        }

        return ApiResponse.fail(String.format("Transaction commit error. %d retry attempts failed", MAX_RETRY_COUNT));

    }

}
