package com.revolut.java.test.api.response;

import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

/**
 * Provides `ok` and `fail` methods that returns JSON prepopulated with status field.
 */
public class ApiResponse {

    private static final Gson GSON = new Gson();

    private final String jsonStringResponse;

    private ApiResponse(ApiResponseStatus apiResponseStatus, String message) {
        jsonStringResponse = GSON.toJson(ImmutableMap.of(
            "status", apiResponseStatus.toString(),
            "message", message
        ));
    }

    private ApiResponse(ApiResponseStatus apiResponseStatus, Map<String, Object> outcome) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("status", apiResponseStatus.toString());
        resp.putAll(outcome);
        jsonStringResponse = GSON.toJson(resp);
    }

    @Override
    public String toString() {
        return jsonStringResponse;
    }

    public static ApiResponse ok(String message) {
        return new ApiResponse(ApiResponseStatus.OK, message);
    }

    public static ApiResponse ok(Map<String, Object> outcome) {
        return new ApiResponse(ApiResponseStatus.OK, outcome);
    }

    public static ApiResponse fail(String message) {
        return new ApiResponse(ApiResponseStatus.FAIL, message);
    }

}
