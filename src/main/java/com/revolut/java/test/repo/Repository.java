package com.revolut.java.test.repo;

import com.google.common.annotations.VisibleForTesting;
import com.revolut.java.test.dto.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Predicate;

/**
 * Repository is a singleton initialized once in {@link com.revolut.java.test.App}.
 * Stores prepared statement and all methods for interaction with the HSQLDB.
 */
public class Repository {

    private static final Logger LOGGER = LoggerFactory.getLogger(Repository.class.getName());

    // queries
    static final String SQL_GET_ACCOUNT_BY_ID = "SELECT id, name, balance FROM account WHERE id = ?";
    static final String SQL_DECREMENT_BALANCE_BY_ID_AND_SUITABLE_BALANCE = "UPDATE account SET balance = balance - ? WHERE id = ? AND balance >= ?";
    static final String SQL_INCREMENT_BALANCE_BY_ID = "UPDATE account SET balance = balance + ? WHERE id = ?";

    private static final Predicate<String> CONCURRENT_UPDATE_INDICATOR = (msg) -> msg.contains("Concurrent update");
    public static final String TRANSFER_CONCURRENT_UPDATE_RESPONSE = "Concurrent update";

    // singleton instance
    private static Repository instance;

    // connection
    private Connection connection;

    // statements
    private final PreparedStatement getAccountByIdStatement;
    private final PreparedStatement decrementBalanceByIdAndSuitableBalance;
    private final PreparedStatement incrementBalanceById;

    @VisibleForTesting
    int twoSeconds = 2000;

    private Repository(Connection connection) throws SQLException {
        this.connection = connection;
        getAccountByIdStatement = connection.prepareStatement(SQL_GET_ACCOUNT_BY_ID);
        decrementBalanceByIdAndSuitableBalance = connection.prepareStatement(SQL_DECREMENT_BALANCE_BY_ID_AND_SUITABLE_BALANCE);
        incrementBalanceById = connection.prepareStatement(SQL_INCREMENT_BALANCE_BY_ID);
    }

    public static Repository getInstance() {
        return instance;
    }

    public static Repository initialize(Connection connection) {
        try {
            instance = new Repository(connection);
        } catch (SQLException e) {
            LOGGER.error("Repository initialization failed!", e);
        }
        return instance;
    }

    // methods

    /**
     * Returns account or null if no account with this id exists.
     */
    public Account getAccount(Integer accountId) throws RepositoryException {

        try {
            getAccountByIdStatement.setInt(1, accountId);
            try (ResultSet result = getAccountByIdStatement.executeQuery()) {
                if (result.next()) {
                    return new Account(
                            result.getInt("id"),
                            result.getString("name"),
                            result.getLong("balance")
                    );
                }
            }
        } catch (SQLException e) {
            throw new RepositoryException("Account couldn't be requested!", e);
        }

        return null;
    }

    /**
     * Executes transfer and return null if success, otherwise returns error message.
     * <p>
     * (!) There are several options to guarantee (C)onsistency:
     * - use connection pool and instantiate prepared statements instead of using static fields
     * - (current: easier for test task purposes) use synchronized methods and provide performance by scalability support
     */
    public synchronized String transfer(Integer fromAccountId, Integer toAccountId, Integer amount) {

        try {

            sleep2Seconds(); // long operation emulation

            decrementBalanceByIdAndSuitableBalance.setInt(1, amount);
            decrementBalanceByIdAndSuitableBalance.setInt(2, fromAccountId);
            decrementBalanceByIdAndSuitableBalance.setInt(3, amount);

            int affectedRowsCount = decrementBalanceByIdAndSuitableBalance.executeUpdate();
            if (affectedRowsCount < 1) { // assuming not enough balance
                connection.rollback();
                return "No account to withdraw money or not enough balance";
            }

            sleep2Seconds(); // long operation emulation

            incrementBalanceById.setInt(1, amount);
            incrementBalanceById.setInt(2, toAccountId);
            affectedRowsCount = incrementBalanceById.executeUpdate();
            if (affectedRowsCount < 1) {
                connection.rollback();
                return "No destination account found";
            }

            sleep2Seconds(); // long operation emulation

            connection.commit();

        } catch (SQLException e) {

            try {
                connection.rollback();
            } catch (SQLException e1) {
                // pass
            }

            if (CONCURRENT_UPDATE_INDICATOR.test(e.getMessage())) {
                LOGGER.warn("Concurrent update caught", e);
                return TRANSFER_CONCURRENT_UPDATE_RESPONSE;
            } else {
                LOGGER.error("Exception during database interaction", e);
                return "There are database issues. Please contact support";
            }

        }

        return null;

    }

    // custom exception to wrap sql exceptions

    public class RepositoryException extends java.lang.Exception {

        RepositoryException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private void sleep2Seconds() {
        try {
            Thread.sleep(twoSeconds);
        } catch (InterruptedException e) {
            LOGGER.error("Something wrong with threads", e);
        }
    }

}
