package com.revolut.java.test.dto;

public class Account {

    private Integer id;
    private String name;
    private Long balance;

    public Account(Integer id, String name, Long balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getBalance() {
        return balance;
    }

}
