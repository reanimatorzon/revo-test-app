CREATE TABLE IF NOT EXISTS account (
   id INTEGER IDENTITY NOT NULL,
   name VARCHAR(127) NOT NULL,
   balance INT NOT NULL,
   PRIMARY KEY (id),
   CONSTRAINT cons_account_balance_positive CHECK (balance >= 0)
);

INSERT INTO account (name, balance) VALUES ('John Doe', 1000);
INSERT INTO account (name, balance) VALUES ('Jane Doe', 1200);
INSERT INTO account (name, balance) VALUES ('Mickey Mouse', 600);
INSERT INTO account (name, balance) VALUES ('Minnie Mouse', 1200);

COMMIT;