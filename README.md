# Revolut Java Test Problem
*Author: Vasiliy Bolgar*

Initial problem is described here - https://drive.google.com/file/d/0B5UMc7RikYaER2JLQUd6Q0xjYnM/view

## Preconditions

- 4-8 hours are enough for completing this task 
- KISS

## Deliverables

- Source code available here (Java 8)
- Binaries: two copy of the same jar to execute in parallel - https://bitbucket.org/reanimatorzon/revo-test-app/downloads/
- API documentation and Usage example: located in this README.md below.
  If you have Java JRE, you can use any browser to run and test the API  
- List of limitations, because that's can't be final solution in declared timings

## Assumptions / Limitations

- There is no dependency injection in any form (even service registry). 
  Only static classes or singletons initialized in one entry point - App.class
- There is only one unit test just for demonstration purposes
- There are no ORM or even a script centralized storage. Only SQL in static constants
- There is no rocket science with multithreading, no connection pool.
  **Nevertheless**, there are transactions that provided consistency
  and this application is cloud-ready. 
  That means it supports multiply instances connected to one database (or database cluster)
  running concurrently
- API is not RESTful. To be honest, I believe it shouldn't be.
  Hard to imagine, how money transfer operation should be mapped on one particular resource.
  I am open for discussion here
- Only GET requests are used for ease of testing
- No request queue implemented on API level. No journaling.
  That's might be the first candidate for next development iteration
- Log messages destination is System.err

## API

| Endpoint                       | Description                | Example                                    | Response                                                              |
|--------------------------------|----------------------------|--------------------------------------------|-----------------------------------------------------------------------|
| /v1/account/:id/balance        | Request balance of account | http://localhost:9001/v1/account/1/balance | {"balance":100,"name":"John Doe","status":"OK"}                       |
| /v1/transfer/:from/:to/:amount | Perform money transfer     | http://localhost:9002/v1/transfer/1/2/900  | {"status":"OK","message":"Successfully transferred"}                  |                                                          
| ...                            | ...                        | ...                                        | {"status":"FAIL","message":"Money transaction failed cause ..."}      |

## Usage
- Download both jars from https://bitbucket.org/reanimatorzon/revo-test-app/downloads/
- `java -jar 1_revolut-test-app-1.0-SNAPSHOT.jar 9001` (run on port 9001)
- `java -jar 2_revolut-test-app-1.0-SNAPSHOT.jar 9002` (run on port 9002)
- Play with endpoints described in the API block.  
*There are three thread sleeps by 2 seconds in the method of money transfer to test parallel money withdrawal.* 
- You have initially (and finally) four accounts in the database:
    + id:1, name:'John Doe', balance:1000 
    + id:2, name:'Jane Doe', balance:1200
    + id:3, name:'Mickey Mouse', balance:600
    + id:4, name:'Minnie Mouse', balance:1200
- (Optional) Remove revolut.mv.\* files from directory with jars to drop H2 database and start from initials
